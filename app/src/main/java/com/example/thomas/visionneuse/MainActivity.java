package com.example.thomas.visionneuse;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ImageView imageView = findViewById(R.id.imagePrincipal);
        Picasso.with(this)
                .load("http://www.denis-fremond.com/photos_art/20062014195748u38892506.JPG")
                .fit()
                .centerInside()
                .into(imageView);

        Typeface kavivanarFont = Typeface.createFromAsset(getAssets(),
                "font/Kavivanar-Regular.ttf");
        TextView textView = findViewById(R.id.popup);
        textView.setTypeface(kavivanarFont);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.avion:
                TextView textView = findViewById(R.id.popup);
                textView.setVisibility(View VISIBLE);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
